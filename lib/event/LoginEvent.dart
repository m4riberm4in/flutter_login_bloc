import 'package:meta/meta.dart';
import 'package:flutter/widgets.dart';

abstract class LoginEvent{}

class LoginButtonPressed extends LoginEvent{
  final String email;
  final String password;

  LoginButtonPressed({
    @required this.email,
    @required this.password,
  });
}

class LoggedIn extends LoginEvent{}
class EmptyField extends LoginEvent{}