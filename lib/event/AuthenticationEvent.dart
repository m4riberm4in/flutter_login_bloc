import 'package:lesson_bloc/models/User.dart';
import 'package:meta/meta.dart';
import 'package:flutter/widgets.dart';

abstract class AuthenticationEvent {}

class AppStarted extends AuthenticationEvent {}

class LoggedIn extends AuthenticationEvent {
  final String token;
  final User user;

  LoggedIn({@required this.token, @required this.user});
}

class LoggedOut extends AuthenticationEvent {}  

class Loading extends AuthenticationEvent {
  final bool isLoading;

  Loading({@required this.isLoading});
}  
