import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';

import 'package:lesson_bloc/models/User.dart';

part 'serializer.g.dart';

@SerializersFor(const [
  User,
])

final Serializers serializers = _$serializers;