import 'package:flutter/material.dart';
import 'package:lesson_bloc/bloc/AuthenticationBloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lesson_bloc/bloc/LoginBlocEvent.dart';
import 'package:lesson_bloc/event/LoginEvent.dart';
import 'package:lesson_bloc/provider/BlocProvider.dart';
import 'package:lesson_bloc/state/LoginState.dart';

class LoginView extends StatefulWidget {
  
  final LoginBlocEvent _loginBloc;
  final AuthenticationBloc _authBloc;

  LoginView({
    Key key,
    @required LoginBlocEvent loginBloc,
    @required AuthenticationBloc authBloc
  }) : _loginBloc = loginBloc,
        _authBloc = authBloc,
        super(key:key);

   @override
   State<LoginView>createState() {
     return _LoginViewState(authBloc: this._authBloc,loginBloc: this._loginBloc);
   }
}


class _LoginViewState extends State<LoginView>{

  final LoginBlocEvent _loginBloc;
  final AuthenticationBloc _authBloc;
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  bool _loginSucceded(LoginState loginState) => loginState.token.isNotEmpty;
  bool _loginFailed(LoginState loginState) => loginState.error.isNotEmpty;


  void _onWidgetDidBuild(Function callback) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        callback();
      });
    }

  _LoginViewState({
    @required LoginBlocEvent loginBloc,
    @required AuthenticationBloc authBloc
  }) : _loginBloc = loginBloc,
      _authBloc = authBloc;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginEvent, LoginState>(
        bloc: _loginBloc,
        builder: (BuildContext context, LoginState loginState) {
          print('this is loginState : ${loginState}');
          if(_loginSucceded(loginState)){
            
            _authBloc.onLogin(token: loginState.token, user: loginState.user);
            _loginBloc.onLoginSuccess();
             _onWidgetDidBuild((){
               Navigator.of(context).pushReplacementNamed('/dashboard');
              //Navigator.of(context).pushNamed('/dashboard');
            });
          }

          if(_loginFailed(loginState)){
            _onWidgetDidBuild((){
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    duration:Duration(seconds: 1),
                    backgroundColor: Colors.red,
                    content: Text(loginState.error)
                  )
                );
            });
          }

          return buildForm('Login Page', context, _emailController, _passwordController, _loginBloc, loginState);
        },
    );
    // TODO: implement build
  }

}

// MaterialApp generate(_title, context, emailController, passwordController){
//   return MaterialApp(
//     title:_title,
//     home: buildScaffold(_title, context, emailController, passwordController)
//   );

// }

// Scaffold buildScaffold(_title, context, emailController, passwordController, LoginBlocEvent _loginBloc, LoginState loginState){
  
//   return Scaffold(

//     body:SafeArea(
//       child:buildForm(_title, context, emailController, passwordController, _loginBloc, loginState),//_buildLayout(_formKey),
//     )
//   );
// }

Form buildForm(_title, context, emailController, passwordController, LoginBlocEvent _loginBloc, LoginState loginState){
  return Form(
    key: Key(_title),
    child:_buildLayout(_title, context, emailController, passwordController, _loginBloc, loginState)
  );
}

Container _buildLayout(_title, context, emailController, passwordController, LoginBlocEvent _loginBloc, LoginState loginState){
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),

    decoration: BoxDecoration(
      image: DecorationImage(
        image: AssetImage('assets/images/bg.png'),
        fit: BoxFit.cover
      )
    ),
    child: Stack(

      children: <Widget>[
          Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                
                children: <Widget>[
                  Text(
                    'Email',
                      style: TextStyle( color: Colors.white),
                  ),
                  TextFormField(
                    controller: emailController,
                    validator: (value){
                      if(value.isEmpty){
                        return 'Please enter some text';
                      }
                      else{

                        
                      }
                    }
                    ),
                    
                    Text(
                    'Password',
                      style: TextStyle( color: Colors.white),
                      
                  ),
                  TextFormField(
                    controller: passwordController,
                    obscureText: true,
                    validator: (value){
                      if(value.isEmpty){
                        return 'Please enter some text';
                      }
                    }
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: () async {
                              if(!loginState.isLoading){
                                _loginBloc.onLoginPressed(email: emailController.text,
                                password: passwordController.text);
                                // if(result){
                                //   showDialog(context: context, builder: (BuildContext context){
                                //       return AlertDialog(content: Text('data ${emailController.text}'));
                                //   });
                                // }
                              }


                          },
                          child: Text('Submit'),
                        )
                      ],
                    ),
                ],
              ),
              Center(
                      child:  loginState.isLoading ? CircularProgressIndicator(backgroundColor: Colors.redAccent) : null
              )
      ],
    )
  );
}







