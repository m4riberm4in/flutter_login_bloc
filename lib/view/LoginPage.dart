import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:lesson_bloc/bloc/AuthenticationBloc.dart';
import 'package:lesson_bloc/bloc/LoginBlocEvent.dart';
import 'package:lesson_bloc/view/Login/Login.dart';
//import 'package:lesson_bloc/view/LoginForm.dart';


class LoginPage extends StatefulWidget {
  @override
  State<LoginPage> createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final LoginBlocEvent _loginBloc = LoginBlocEvent();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Login'),
      // ),
      body: LoginView(
        
        authBloc: BlocProvider.of<AuthenticationBloc>(context),
        loginBloc: _loginBloc,
      ),
    );
  }

  @override
  void dispose() {
    _loginBloc.dispose();
    super.dispose();
  }
}