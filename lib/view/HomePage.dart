

import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:lesson_bloc/bloc/AuthenticationBloc.dart';
import 'package:lesson_bloc/event/AuthenticationEvent.dart';
import 'package:lesson_bloc/models/User.dart';
import 'package:lesson_bloc/state/AuthenticationState.dart';

class HomePage extends StatelessWidget{


  bool isAuth(AuthenticationState authState) => authState.isAuthenticated;
  User getUser(AuthenticationState authState) => authState.user;


 void _onWidgetDidBuild(Function callback) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        callback();
      });
    }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    final AuthenticationBloc authenticationBloc = 
        BlocProvider.of<AuthenticationBloc>(context);

      print(authenticationBloc.toString());
      return BlocBuilder<AuthenticationEvent, AuthenticationState>(
        bloc:authenticationBloc,
        builder: (BuildContext context, AuthenticationState authState){
          print('event ${authState}');

          // if(authState.isLogout){
          //     _onWidgetDidBuild((){
          //         Navigator.of(context).pushReplacementNamed('/login');
          //     });
          //   }

            
          return Scaffold(
                  appBar: AppBar(
                    title: Text('Home'),
                  ),
                  body: Stack(
                    children: <Widget>[
                        Center(
                          child: RaisedButton(
                            child: Text('Logout'),
                            onPressed: (){
                              print('logout in button');
                              authenticationBloc.onLogout();
                              _onWidgetDidBuild((){
                                  Navigator.of(context).pushReplacementNamed('/login');
                              });
                            },
                          ),
                        ),
                        Center(
                                child:  authState.isLoading ? CircularProgressIndicator(backgroundColor: Colors.redAccent) : null
                        )
                    ],
                    
                  ),
                );
              },
      );
  }

}