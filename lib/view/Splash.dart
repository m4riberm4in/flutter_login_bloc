
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:path/path.dart';

class Splash extends StatefulWidget{

  bool nav = false;
  final timeout = const Duration(seconds: 3);
  BuildContext _context;
  
  Splash({Key key, BuildContext context}) : 
        _context = context;

  State<Splash> createState() => SplashState();
}

  class SplashState extends State<Splash>{


    Future time(int time) async {

      Completer c = new Completer();
      new Timer(new Duration(seconds: time), (){
        c.complete('done with time out');
        //Navigator.of(this.context).pushNamed('/login');
      });

      return c.future;
    }

  @override
  void initState(){
      super.initState();
      print('init');
      time(3).then((val) => Navigator.of(this.context).pushReplacementNamed('/login'));
    }
    
  @override
  void dispose(){
    super.dispose();
  }

  @override
    Widget build(BuildContext context) {
      
      
      //TODO: implement build
      return Scaffold(
        body: Center(
          child: Text('Splash Page here'),
        ),
      );
    }

    

    void _onWidgetDidBuild(Function callback) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        callback();
      });
    }

  }

  

