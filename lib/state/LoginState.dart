import 'package:lesson_bloc/models/User.dart';
import 'package:meta/meta.dart';


class LoginState{
  final bool isLoading;
  final bool isLoginButtonEnabled;
  final String error;
  final String token;
  final User user;

 const LoginState({
    @required this.isLoading,
    @required this.isLoginButtonEnabled,
    @required this.error,
    @required this.token, 
    this.user
  });

  factory LoginState.initial() {
    return LoginState(
      isLoading: false,
      isLoginButtonEnabled: true,
      error: '',
      token: '',
      user: null
    );
  }

  factory LoginState.loading() {
    return LoginState(
      isLoading: true,
      isLoginButtonEnabled: false,
      error: '',
      token: '',
      user: null
    );
  }

  factory LoginState.failure(String error) {
    return LoginState(
      isLoading: false,
      isLoginButtonEnabled: true,
      error: error,
      token: '',
      user: null
    );
  }

  factory LoginState.success(String token, User user) {
    return LoginState(
      isLoading: false,
      isLoginButtonEnabled: true,
      error: '',
      token: token,
      user: user
    );
  }

  LoginState copyWith({
    bool isLoading,
    bool isLoginButtonEnabled,
    String error,
    String token,
    User user
  }) {
    return LoginState(
      isLoading: isLoading ?? this.isLoading,
      isLoginButtonEnabled: isLoginButtonEnabled ?? this.isLoginButtonEnabled,
      error: error ?? this.error,
      token: token ?? this.token,
      user : user ?? this.user
    );
  }

  @override
  String toString() =>
      'LoginState { isLoading: $isLoading, isLoginButtonEnabled: $isLoginButtonEnabled, error: $error, token: $token }';
}