
import 'dart:async';

import 'package:lesson_bloc/provider/BlocProvider.dart';

class LoginBloc implements BlocBase {
  
  String email;
  String password;

  final validateUsername = StreamTransformer<String, String>.fromHandlers(
    handleData: (email, sink){
      if(email.contains('@')){
        sink.add(email);
      }
      else{
        sink.addError('Not valid email');
      }
    }
  );

  StreamController<String> _loginController = StreamController<String>();
  StreamSink<String> get _inAction => _loginController.sink;
  Stream<String> get outAction => _loginController.stream.transform(validateUsername);

  StreamController _actionController = StreamController();
  StreamSink get actionController => _actionController.sink;


  LoginBloc(){
    _actionController.stream.listen(handleLogic);
  }
  
  handleLogic(data){
    // switch (data.action) {
    //   case 'submit':

    //   break;
    //   case 'type' :
    //       email = data.email;
    //       password = data.password;
    //   break;
    //   default:
    // }
    print(data);
    _inAction.add(data);
  }

    @override
  void dispose() {
    // TODO: implement dispose
    _actionController.close();
    _loginController.close();
  }

}