
import 'dart:async';

import 'package:lesson_bloc/provider/BlocProvider.dart';

class IncrementBloc implements BlocBase {


  int _counter = 0;
  int _evenNumber = 0;

  final evenNumber = StreamTransformer<int, int>.fromHandlers(
    handleData: (number, sink){
      if(number % 2 == 0){
        sink.add(number);
      }
      else{
        sink.addError('Not A Even Number');
      }
    }
  );

  StreamController<int> _counterController = StreamController<int>();
  StreamSink<int> get _inAction => _counterController.sink;
  Stream<int> get outAction => _counterController.stream.transform(evenNumber);

  StreamController _actionController = StreamController();
  StreamSink get incrementCounter => _actionController.sink;


  IncrementBloc(){
    _counter = 0;
    _evenNumber = 0;
    _actionController.stream.listen(handleLogic);
  }
  
  handleLogic(data){
    if(data == 'add'){
      ++_counter;
    }
    else{
      --_counter;
    }
    _inAction.add(_counter);
  }

    @override
  void dispose() {
    // TODO: implement dispose
    _actionController.close();
    _counterController.close();
  }

}