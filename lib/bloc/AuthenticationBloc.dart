import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:lesson_bloc/models/User.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'package:lesson_bloc/event/AuthenticationEvent.dart';
import 'package:lesson_bloc/state/AuthenticationState.dart';
import 'package:path/path.dart';

class AuthenticationBloc extends Bloc<AuthenticationEvent, AuthenticationState>{


  Future<bool> _hasToken() async {
    /// read from keystore/keychain
    await Future.delayed(Duration(seconds: 1));
    return false;
  }

  Future<void> _deleteToken() async {
    /// delete from keystore/keychain
    await Future.delayed(Duration(seconds: 1));
    return;
  }

  Future<void> _persistToken(String token) async {
    /// write to keystore/keychain
    await Future.delayed(Duration(seconds: 1));
    return;
  }

  @override
  AuthenticationState get initialState => AuthenticationState.initializing();
  
  @override
  Stream<AuthenticationState> mapEventToState(AuthenticationState state, AuthenticationEvent event) async* {
    // TODO: implement mapEventToState
    if(event is AppStarted){
      final bool _hasToken = await this._hasToken();
      print(_hasToken);
      if(_hasToken){
        yield AuthenticationState.authenticated();
      }
      else{
        yield AuthenticationState.unauthenticated();
        //yield state.copyWith(isAuthenticated: false, isLoading: false);
        
      }
    }

    if(event is LoggedIn){
      yield state.copyWith(isLoading: true);
      await _persistToken(event.token);
      yield state.copyWith(isLoading:false, isAuthenticated:true, user:event.user);
      //yield AuthenticationState.authenticated();
    }

    if(event is LoggedOut){
      print('event ${event}');
      yield state.copyWith(isLoading: true, isLogout: true);

      await _deleteToken();
      yield AuthenticationState.unauthenticated();
    }

    if(event is Loading){
      print('event is loading ${event.isLoading}');
      yield state.copyWith(isLoading: event.isLoading);
    }
    
  }

  void onAppStart(){
    dispatch(AppStarted());
  }

  void onLoading({@required bool isLoading}){
    dispatch(Loading(isLoading:isLoading));
  }

  void onLogin({@required String token, @required User user}){
    dispatch(LoggedIn(token:token, user:user));
  }

  void onLogout(){
    print('dispatch loggout');
    dispatch(LoggedOut());
  }



}