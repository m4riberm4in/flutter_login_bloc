
import 'package:lesson_bloc/api/ServicesApi.dart';
import 'package:lesson_bloc/models/User.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:lesson_bloc/event/LoginEvent.dart';
import 'package:lesson_bloc/state/LoginState.dart';
import 'package:lesson_bloc/serializer/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'dart:async';
import 'dart:convert';

class LoginBlocEvent extends Bloc<LoginEvent, LoginState>{

  LoginState get initialState => LoginState.initial();

  Future onLoginPressed({String email, String password}) async {
    if(email.isEmpty || password.isEmpty){
      dispatch(EmptyField());
      return false;
    }
    else{
      dispatch(LoginButtonPressed(
                email: email, 
                password: password
              ));
      return true;
    }

    
  }

  void onLoginSuccess(){
    dispatch(LoggedIn());
  }


  @override
  Stream<LoginState> mapEventToState(LoginState state, LoginEvent event) async* {
    // TODO: implement mapEventToState
    print(event);
    if(event is LoginButtonPressed){
      yield LoginState.loading();
      
      try {
          final token = await this._getToken(
            email: event.email,
            password: event.password
          );

        final user = await ServicesApi.getAuthAPI().doLogin(event.email, event.password);
        yield LoginState.success(token, user);

      } catch (e) {
        print('error ${e}');
        yield LoginState.failure(e.toString());
      } 
    }

    if(event is LoggedIn){
      yield LoginState.initial();
    }

    if(event is EmptyField){
      yield LoginState.failure('Field Empty');
    }
  }

  Future<String> _getToken({
    @required String email,
    @required String password,
    
  }) async {
    await Future.delayed(Duration(seconds: 1));

    /// uncomment the following line to simulator a login error.
    // throw Exception('Login Error');
    return 'token';
  }

  // Future<User> _getUser() async {
  //   final standardSerializers = (serializers.toBuilder()..addPlugin(new StandardJsonPlugin())).build();
  //   final resJson = jsonDecode('{ "firstname" : "John", "lastname" : "Doe", "email" : "ini email" }');
  //   User user = standardSerializers.deserializeWith(User.serializer, resJson);
  //   return user;

  // }

}