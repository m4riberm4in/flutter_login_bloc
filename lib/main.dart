import 'package:flutter/material.dart';
//import 'package:lesson_bloc/provider/BlocProvider.dart';
// import 'package:lesson_bloc/bloc/LoginBloc.dart';
// import 'package:lesson_bloc/bloc/IncrementalBloc.dart';
// import 'package:lesson_bloc/view/Login/Login.dart';


import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lesson_bloc/bloc/AuthenticationBloc.dart';
import 'package:lesson_bloc/event/AuthenticationEvent.dart';
import 'package:lesson_bloc/state/AuthenticationState.dart';
import 'package:lesson_bloc/view/HomePage.dart';
import 'package:lesson_bloc/view/LoginPage.dart';
import 'package:lesson_bloc/view/Splash.dart';

void main() => runApp(new App());

class App extends StatefulWidget {
  @override
  State<App> createState() => AppState();
}

class AppState extends State<App> {
  final AuthenticationBloc _authenticationBloc = AuthenticationBloc();

  @override
  void initState(){
    super.initState();
    
    //WidgetsBinding.instance.addPostFrameCallback((_) => showOverlay());
  }


  @override
  void didUpdateWidget(Widget oldWidget) {
    print(oldWidget);
  // if (oldWidget.importantProperty != widget.importantProperty) {
  //   _init();
  // }
}

  AppState() {
    _authenticationBloc.onAppStart();
  }

  @override
  void dispose() {
    _authenticationBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthenticationBloc>(
      bloc: _authenticationBloc,
      child: MaterialApp(
        home: _rootPage(),
        routes:<String, WidgetBuilder>{
          '/login':(BuildContext context){
            return LoginPage();
          },
          '/dashboard':(BuildContext context){
            return HomePage();
          }
        },
      ),
    );
  }

  Widget _rootPage() {
      List<Widget> widgets = [];
      
    return BlocBuilder<AuthenticationEvent, AuthenticationState>(
      bloc: _authenticationBloc,
      builder: (BuildContext context, AuthenticationState state) {
            print('all state ${state}');
              if (state.isAuthenticated) {
                print('home page');
                //widgets.add(HomePage());
                if(widgets.length > 0)
                  widgets.removeLast();

              } else {
                print('login page $widgets');
                if(widgets.length > 0)
                  widgets.removeLast();
                //widgets.add(LoginPage());
                
              }

              if (state.isInitializing) {
                print('initializing');
                //widgets.add(Splash(context: context));
              }

              if (state.isLoading) {
                //widgets.add(_loadingIndicator());
              }
        return SafeArea(
          child: Stack(
          // children: widgets,
          children: <Widget>[
            Splash(context: context)
          ],
          )
        );
      },
    );
  }

  Widget _loadingIndicator() {
    return Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.3,
          child: ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        Center(
          child: CircularProgressIndicator(),
        ),
      ],
    );
  }
}





// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return new MaterialApp(
      
//       title: 'Tutorial Stream',
//       theme: new ThemeData(
//         // This is the theme of your application.
//         //
//         // Try running your application with "flutter run". You'll see the
//         // application has a blue toolbar. Then, without quitting the app, try
//         // changing the primarySwatch below to Colors.green and then invoke
//         // "hot reload" (press "r" in the console where you ran "flutter run",
//         // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
//         // counter didn't reset back to zero; the application is not restarted.
//         primarySwatch: Colors.blue,
//       ),
//       //home: new StreamCounterPage(title: 'Flutter Demo Home Page'),
//       home: BlocProvider<LoginBloc>(
//         bloc: LoginBloc(),
//         child: LoginView(title: 'Login'),
//       ),
//     );
//   }
// }

// class StreamCounterPage extends StatelessWidget {
//   StreamCounterPage({Key key, this.title}) : super(key: key);

//   final String title;

//   // @override
//   // StreamCounterPageState createState() => new StreamCounterPageState();
  
//   @override
//   Widget build(BuildContext context){
//       final IncrementBloc bloc = BlocProvider.of<IncrementBloc>(context);
//       return Scaffold(
//         appBar: AppBar(
//           title: Text('Test Streams'), 
//         ),
//         body: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Text('Pushed The Button'),
//               StreamBuilder<int>(
//                 initialData: 0,
//                 stream: bloc.outAction,
//                 builder: (BuildContext context, AsyncSnapshot<int> snapshot){

//                   return buildText(context, snapshot);
//                 }
//               ),
//             ],
//           ),
//         ),
//         floatingActionButton: Column(
//           mainAxisAlignment: MainAxisAlignment.end,
//           children: <Widget>[
            
//               FloatingActionButton(
//                 onPressed: (){
//                   bloc.incrementCounter.add('add');
//                 },
//                 tooltip: 'Increment',
//                 child: const Icon(Icons.add),
//               ),
//               FloatingActionButton(
//                 onPressed: (){
//                   bloc.incrementCounter.add('substract');
//                 },
//                 tooltip: 'Increment',
//                 child: Text('-', style:Theme.of(context).textTheme.display1),
//               ),
              
//           ],
//         )
//       );
//     }

// }

// Text buildText(context, snapshot){
//     return Text('hello ${snapshot.data}', style: Theme.of(context).textTheme.display1);
// }
