const BASE_URL = 'https://localhost/';
const BASE_URL_API = 'https://devproxy.astra.co.id/';
const BASE_URL_LOGIN = "https://accounts.astra.co.id/";

const ENDPOINT= {
  'SUBMIT_FEEDBACK_COMPLAINT':'mobilefswebapi/api/postfeedbacksubmitproblem',
  'SUBMIT_PROBLEM_ITSS':'mobilefswebapi/api/postrequestsubmitproblem',
  'SUBMIT_PROBLEM_GASS':'problemmanagementwebapi/api/submitcomplaint',
  'GET_COMPLAINT_DETAIL_GASS':'problemmanagementwebapi/api/getcomplaintdetail',
  'GET_COMPLAINT_DETAIL_ITSS':'mobilefswebapi/api/getinboxdetail',
  'MASTER_DATA':'smartofficewebapi/api/getmasterdata',
  'GET_LANDING_DATA':'smartofficewebapi/api/getlandingdata',
  'GET_USER_INFO':'smartofficewebapi/api/getuserinfo',
  'GET_AVAILABLE_ROOM':'roomreservationwebapi/api/getavailablerooms',
  'SUBMIT_BOOKING':'roomreservationwebapi/api/submitbooking',
  'UPDATE_BOOKING':'roomreservationwebapi/api/updatebooking',
  'CANCEL_BOOKING':'roomreservationwebapi/api/cancelbooking',
  'GET_BOOKING_DETAIL':'roomreservationwebapi/api/getbookingdetail',
  'GET_PARTICIPANTS':'smartofficewebapi/api/getparticipants',
  'GET_AVAILABLE_MAP':'roomreservationwebapi/api/getavailableroomsformap',
  'GET_MASTER_DATA_ITSS' :'mobilefswebapi/api/getmasterdata',
  'GET_MASTER_DATA_GASS ':'',
  'LOGIN':'oauth2/token',
  'LOGOUT':'oauth2/revoke'
};

const PUBKEY_LOCALSTORE = 'Astra_123@111';
const PUBLIC_KEY = '';
const PRIVATE_KEY = '';
const BASIC_KEY = 'aGFXZG5sc2FEcUhmSWhGajBDMHhvNnJvV2hrYTowc3RkSnpNMGJLakFmZHVPN050WTRHSmJseVFh';
