import 'dart:convert';

import 'package:lesson_bloc/models/User.dart';
import 'package:http/http.dart' as http;
import 'package:lesson_bloc/api/ApiList.dart';
import 'package:lesson_bloc/serializer/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

class ServicesApi {

  static ServicesApi servicesApi;
  final standardSerializers = (serializers.toBuilder()..addPlugin(new StandardJsonPlugin())).build();
  User user;

  static ServicesApi getAuthAPI(){
    if(servicesApi == null){
      servicesApi = new ServicesApi();
    }
    return servicesApi;
  }

  Future<User> doLogin(email, password) async {
    print("call do login : ${email} - ${password}");
    
      final response = await http.post(
          BASE_URL_LOGIN+ENDPOINT['LOGIN'],
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + BASIC_KEY
          },
          body: "username=$email&password=$password&scope=openid&grant_type=password"
        ).then((res){
            print('res ${res}');
            if(res.statusCode == 200){
              print("Success Login : ${res.body}");
              final resJson = jsonDecode(res.body);
              user = standardSerializers.deserializeWith(User.serializer, resJson);
              return user;
            }
            else{
                print("Failed Login : ${res.body}");
                throw res.body;
            }
        }).catchError((onError)=>throw(onError));

        return response;
  }

}